# ImageClassifier - MiniX10
**Gruppe 5 - Maise, Mads, Job og Amalie**
___


**Link til kørende program:** https://sofieamaliem.gitlab.io/aestetisk-programmering/MiniX10/index.html 


**Link til source code:** https://gitlab.com/sofieamaliem/aestetisk-programmering/-/blob/main/MiniX10/sketch.js


![](/MiniX10/Skærmbillede1.png)
![](/MiniX10/Skærmbillede2.png)
![](/MiniX10/Skærmbillede3.png)


##### Hvilken eksempel kode har I valgt, og hvorfor?


Vi har valgt at tage ImageClassifier. Dette gjorde vi fra flere forskellige grunde. Vi synes det var spændende at arbejde med machine learning inden for klassifikation af billeder. Ydermere bruger ImageClassifier også ImageNets database, hvor der er mere end 15 millioner forskellige billeder, der er blevet klassificeret af mennesker gennem Amazon Mechanical Turk.


Mechanical Turk er en crowdsourcing hjemmeside, hvor virksomheder kan betale villige arbejdere pengebeløb for at udføre opgaver, som computere af den ene eller anden grund ikke kan - i dette tilfælde at klassificere billeder for at træne et neuralt netværk. Dermed er programmet trænet af forskellige mennesker rundt på kloden, og dens database er således påvirket af deres syn. Vi tænkte derfor det var spændende at prøve at se, hvor akkurat den var, og hvor meget BIAS der er i programmet.


##### Har I ændret noget i koden for at forstå programmet? Hvilke? Hvilke linjer af kode har været særlig interessante for gruppen? Hvorfor? Var der syntakser/funktioner, som I ikke kendte til før? Hvilke? Og hvad lærte I?


Vi forstod koden som den var, men vi har optimeret den for at vi i dybere grad kan forstå den. For at optimere programmet har vi tilføjet `funktionen push() and pop()`, som gør at teksten og billedets størrelse ikke bliver forstyrret af hinanden. Vi har ligeledes ændrede canvas samt tilføjede egne billeder for at få en bredere forståelse af helheden i programmet. Vi synes at `‘div’-funktionen`  var interessant, da man på meget lidt plads, kunne generere et tekststykke i en funktion uden at bruge tekstfunktionen. Ligeledes synes vi det er interessant at de har valgt at sætte .img til en global variabel, som hele tiden skal rettes i, i funktion preload og dermed ikke gør brug af en nemmere metode, som at sætte alle billederne ind i et array. Dette har vi derfor tilføjet til kode for at gøre den mere håndgribelig.


#####  Hvordan ville I udforske/udnytte begrænsningerne af for eksempel koden eller machine learning algorithms? Hvordan kan I se en større relation mellem eksempel koden, som I har undersøgt, og brugen af machine learning ude i verdenen (fx. kreative AI, Voice Assistance, selvkørende biler, bots, ansigtsgenkendelse osv.)?


For at udforske de begrænsninger, eksempel koden fremviser, tilføjer vi flere billeder til databasen for at undersøge hvor godt den er trænede til at genkende/kategorisere diverse billeder korrekt i forhold til hvad de viser. Ved denne metode tester vi machine learning algoritmerne og hvor godt de er trænet til netop denne udførelse. Vi oplever ved denne metode, at der er en del fejl i databasen, det kan skyldes at den er trænede til meget specifikke detaljer på et billede, og dermed vil undgå en generalisering af mennesker. Dette skaber fejl, fordi det fremstår at den opdigter detaljer, som ikke finder sted på billedet.




Hvis man skal se på den større relation mellem koden og machine learning, er det oplagt at kigge på hvilke typer af machine learning der er tale om. Supervised learning er den model der er baseret på et træningsdata-sæt med både et input-par og output-par. Formålet med denne supervised learning er at kortlægge inputdataerne til output etiketter. Det er denne type af machine learning vores valgte eksempelkode benytter.




> _ ” The goal of this type of learning is to map the input data to output labels. For example, with new email as the input, what would the predicted output result be? Can it be classified as spam and then moved to the spam mailbox? […] (Soon, et al. s. 233)_




Måden hvorpå programmet er trænet har også en kæmpe indflydelse på, hvordan billederne bliver opfattet. Programmet ved intet før det får input fra os og vi giver objekterne en karakter, hvorved programmet bliver påvirket af vores subjektive holdninger.


> _Clearly algorithms do not act alone or with magical (totalising) power, but rather exist as part of larger infrastructures and ideologies. (Geoff Cox, “Ways of Machine Seeing,” Unthinking Photography (2016), https://unthinking.photography/articles/ways-of-machine-seeing)_


Vi kan altså se, at algoritmer er styret af ideologier og infrastrukturer, altså menneskers meninger og holdninger kommer til udtryk igennem algoritmerne som kategoriserer de valgte billeder, i bestemte kategorier som ikke er valgt med bevidsthed. Algoritmerne sorterer kategorierne, som er valgt af mennesker der sidder og træner de valgte datasæt. De agendaer og ideologier der bruges, har hermed en betydning for, hvad de forskellige datasæt kommer til at indeholde. Denne fremgangsmåde kan skabe fejl i de forskellige datasæt, som resulterer i at identificeringen af eksempelvis billeder kan være fejlagtige.


> _Data is categorized in a discrete manner, and there are many reasons that might lead to a “normative” prediction and this is especially problematic when it comes to complex subjects such as gender, race, and identity, because these operate beyond binary, discrete classification. (Soon et. al. s. 234)_


#####  Hvad kunne du tænke dig at vide mere om? Eller hvilke spørgsmål kan du ellers formulere ift. emnet?


Vi kunne godt tænke os at vide mere om hvilke kategorier som databasen vælger fra og hvordan den er trænet? Er den sorteret efter vold eller andet, som måske bevidst bliver taget fra. Så mere generelt hvordan databasen er valgt.


### Eksempelkode


Denne filet er en image classification, som benytter MobileNet og p5.js. Denne kode bruger et callback pattern, som er et stykke eksikverbar kode, der sendes som et argument til anden kode, som forventes at kaldes tilbage.


Her laves en globalvariable som kalder på classefieren, der er trænet med MobileNet.


`let classifier;`


En global variable til at holde på det valgte billede som vi vil classificere.


`let img;`


Laver et array der indeholder billederne, der skal kategoriseres.


`let images = [];`


Denne funktion kalder på classefier (tekst), som bliver valgt at den trænede MobileNet, samt kalder på de loadede billeder.


```
function preload() {
 classifier = ml5.imageClassifier('MobileNet');
```
Dernæst preloader vi billederne i array


```
images[0] = loadImage('images/1.jpeg');
   images[1] = loadImage('images/2.jpeg');
   images[2] = loadImage('images/3.jpeg');
   images[3] = loadImage('images/4.jpeg');
   images[4] = loadImage('images/5.jpeg');
   images[5] = loadImage('images/6.jpeg');
   images[6] = loadImage('images/7.jpeg');
   images[7] = loadImage('images/8.jpeg');
   images[8] = loadImage('images/9.jpeg');
   images[9] = loadImage('images/10.jpeg');
   images[10] = loadImage('images/11.jpeg');
   (....)
```
Her laves en lokal variabel, der vælger 1 billeder imellem de 57 billeder. Dette er smart, da det så er det billede vi refferer til resten af funktionen.


```
 createCanvas(500, 500);
 classifier.classify(img, gotResult);
 push();
 img.resize(400, 400);
 image(img, 0, 0);
 pop();


```
Denne funktion der køres når vi får fejl og resultater, samt viser fejl og mangler i console-log.


```
function gotResult(error, results) {
 if (error) {
   console.error(error);
 }
```


Resultatet er i et array, sorteret efter hvor sikker den er på resultatet/billedet. Dette bliver også vist i consolen.


`  console.log(results);`


Disse to generere teksten. Label er hvad den kategorisere/classificere billedet som. Confidence er hvor sikker den er på det den classificer den som. Funktionen opretter et div-element som gør at man kan skrive noget. Funktionen nf bruger talformatering. Til at angive at sikkerheden(confidence) skal formateres med to decimaler. Det vil sige at at der kun bliver vist 0,xx. 0, 2 er placeringen.


```
 createDiv('Label: ' + results[0].label);
 createDiv('Confidence: ' + nf(results[0].confidence, 0, 2))}
```


## Reference liste


https://p5js.org/reference/


Soon, Winnie & Cox, Geoff - Aesthetic programming, A handbook of Software Studies


https://gitlab.com/jobx0012/aepnisgaard/-/tree/main/miniX10


https://gitlab.com/MaiseAlling/maise-gitlab/-/tree/main/MiniX10


https://gitlab.com/Madseladen/aestetiskprogrammering/-/tree/main/MiniX10
