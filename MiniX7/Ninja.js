
class Ninja {
    constructor() {
        this.posX = width / 2;
        this.posY = height / 2;
        this.sizeX = 350;
        this.sizeY = 300;

    }


    show() {
        push()
        imageMode(CENTER);
        image(frugtninja, constrain(mouseX, 0, width), constrain(mouseY, 0, height), this.sizeX, this.sizeY);
        pop()

    }

}