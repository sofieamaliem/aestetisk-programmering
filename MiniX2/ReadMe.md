# ReadMe - MiniX2 

I denne MiniX2 har jeg valgt at skabe et design der har to betydninger. Min ide var at skabe et design hvor forskellige befolkningsgrupper var repræsenteret, men stadig skitseret op på en minimalistisk måde. Jeg har derfor taklet min ide ved at lave en regnbue emoji, hvor jeg har lavet den så den både kan have et neutralt ansigt, men også et kækt et hvor den kan rækker tunge. Den anden emoji jeg har lavet, er en mørk emoji og når man tryker på den bliver den til en gul emoji. Det er både en måde at illustere at vi alle sammen hører under det samme menneske (helt inde i kødet haha) , men også adskillelsen der er imellem. Man kan derfor vælge at tolke helt om man vil på de emojis jeg har skabt :) 

**Link til mit program:** https://sofieamaliem.gitlab.io/aestetisk-programmering/MiniX2/index.html

**Link til min kode:** https://gitlab.com/sofieamaliem/aestetisk-programmering/-/blob/main/MiniX2/MiniX2.js 

![](/MiniX2/Skærmbillede_MiniX2.1.png) ![](/MiniX2/Skærmbillede_MiniX2.3.png)

## Min kode - Regnbue smiley
Jeg har naturligvis startet mit program med at lave størrelsen på mit canvas i `function setup()`: 

`createCanvas(windowWitdth, windowHeight);`


Dernæst har jeg i `function draw()` lavet min baggrundsfarve før jeg sætter mine geometriske former ind. 

Jeg har i denne MiniX2 opgave lave et program ud fra de betingelser der var listet op i opgaven. Min kode består af **2 primære** emojis og **2 sekundære** emojis, da de begge er animerede og kan skifte ansigt. Den første emoji, forstiller en regnbue som er skabt ved at bruge koden:

`ellipse( x, y, w)`

Da ellipsen har samme placering på x og y aksen, har jeg blot ændrede i ellipsens vidde og højde for at skabe regnbue effekten. For at lave den samme afstand mellem hver ellipse der placeres har jeg ændrede vidden med et interval på 30 pixels. 

**Vidden går dermed fra:**

`fill(255,0,0);`
`ellipse(500,400,290);`
 
**Til:** 

 `fill(51,204,0);`
 `ellipse(500,400,20);`

Farvene er valgt ud fra den klassiske regnbue og jeg har brugt fill(R,G,B); til at definere mine farver. 

Jeg har dernæst lavet øjnene på min regnbue, som er lavet af tre omgange for at få flere detaljer på: 

For at lave selve de runde har jeg brugt referencen `circle(x, y, d)`; 


`noStroke();`
`fill(255);`
`circle(530,380,50);`
`circle(470,380,50);`


Jeg har valgt at bruge `noStroke();` for at mine øjne ikke var markerede med en sort kant. 

Dernæst lavede jeg detaljerne i øjnene: 


`fill(0);`
`circle(530,390,30);`
`circle(470,390,30);`



`fill(255,255,255);`
`circle(530,395,18);`
`circle(470,395,18);`


For at lave munden på min emoji har jeg brugt arc-referencen `arc (x, y, h, start, stop, PI); ` og indsatte de værdier der passede til min emojis størrelse: 

**Yderste mund:**

`fill(255);`
`arc(500,450,100,80,0,PI);`

**Animerede tunge:**

For at lave tungen til min emoji brugte jeg samme reference som til munden, men ændrede min start positon til at være mouseY som gør at når jeg bevæger min mus op og ned af y-aksen vil tungen følge med og min emoji vil derfor række tunge: 


`fill(255);`
`arc(500,450,100,mouseY,0,PI);`

Jeg har yderligere valgt at tilføje tekst til min kode, da jeg synes det gav lidt ekstra til mit program og giver det ligeledes lidt humor ;)
Jeg valgte først hvilken størrelse jeg ønskede teksten i: 

`textSize(20);`

Dernæst valgte jeg hvor henne teksten skulle placeres i forholdt til min emoji, hvilken farve teksten skulle have hvad der skulle stå: 

`fill(0);`
`text('Move the tongue & have fun,370,230);`

Jeg brugte her greyscale til at definere min farve. 

## Min kode - Brun emoji

Jeg brugte den samme fremgangsmåde til at designe min brune og gule emoji: 

**Selve emojien:**

`fill(102,51,0);`
`ellipse(1000,400,290);`


Øjnene på emoji som er henholdvsis det hvide runde, den sorte propil og den hvide detalje: 

`fill(255);`
`circle(1030,380,50);`
`circle(970,380,50);`

`fill(0);`
`circle(1030,380,30);`
`circle(970,380,30);`


`fill(255);`
`circle(1030,368,18);`
`circle(970,368,18);`


Jeg brugte ligeledes samme fremgangsmåde til at lave munden på min brune smiley, så den lignede den på regnbue emojien. Jeg tilføjede bare en ekstra mund for at lave en ekstra detalje. 

`fill(255,204,204);`
`arc(1000,440,100,80,0,PI);`
`fill(204,0,51);`
`arc(1000,445,80,60,0,PI);`


For at understrege mit ønske om en fransk emoji tilføjede jeg en hat og overskæg: 

Jeg brugte de samem former som jeg har brugt før ved både at bruge `arc (x, y, h, start, stop, PI); ` og `ellipse( x, y, w)` 

**Hat:**

`fill(255,40,40);`
`ellipse(1000,295,250,30);`
`ellipse(1000,265,300,75);`
`ellipse(1000,240,8,75);`


**Overskæg:**

`fill(0);`

`arc(1000,426,130,10,0,PI);`

Jeg tilføjede ligeledes tekst til denne emoji ved at bruge samme kodereference som før: 

`textSize(20);`
`text('Press to change,940,600);`
`text(if you dare,970,620);`

## Min kode - Gul emoji

For nu at få min brue emoji til at blive gul ved at trykke med musen har jeg brugt et if-statement. Ved at sige at når man trykker på musen er det true og den skal skifte bruger jeg denne kode-reference: 

`if(mouseIsPressed === true){`

Da min brune emoji har en hat og den gule emoji ikke skal have en hat har jeg først sat koden for hatten ind i mit if-statement og fyldt den i samme farve som min baggrund `fill(255,255,204)`. Jeg var i den forbindelse også nødt til at tilføje en `stroke(255,255,204)` på min hat så jeg ikke fik en rød kant. Jeg har placeret min hat bag min gule emoji da programmet vil læse den først og dermed blive fjernet inden den gule emoji kom frem. 

Jeg lavede dernæst min gule emoji på samme måde, som jeg ville gøre hvis den ikke var i et if-statement, da den skal have samme form og størrelse som den smiley der er under. 

**Gul emoji:**

`fill(255,255,0);`
`ellipse(1000,400,290);`

**Øje åbent:**

`fill(255);`
`circle(1030,380,50);`
`fill(0);`
`circle(1030,380,30);`
`fill(0);`
`circle(1030,368,18);`

**Øje lukket:**

`fill(0);`
`arc(950,380,40,5,0,PI);`

**Mund:**

`fill(255);`
`arc(1000,440,100,80,0,PI);`
`fill(204,0,51);`
`arc(1000,460,40,90,0,PI);`


## Min oplevlese med MiniX2

Denne MiniX2 opgave har udfordrede det at kunne forstå og bruge if-statements og bruge variabler, men synes det har været spændende at arbejde med og se hvor mange ting man kan lave hvis blot man bruger de overnævnte kode-typer. Udover synes jeg det har været interessant at have en debat omkring det politiske og æstetisk aspekt i emojis og hvor stor en betydning det har haft for mange mennesker og at det er meget forskelligt. Personligt synes jeg det er lidt i overkanten med alle de emojis der eksistere og havde derfor en anelse svært ved at skulle lave en helt neutral emoji da jeg synes den gule er ret neutral. Jeg synes at min regnbueemoji er en af de emojis der måske kunne mangle, da det jo er ret moderne lige pt at have fokus på netop dette. Jeg har til det også valgt at lave et ret neutralt udtryk i form af øjnene. De henviser hverken til en mand eller en kvinde og man skal derfor ikke identificere sig som noget hvis man bruger den emoji. Ligeledes har jeg lavet den emoji der kan skifte for at kridte det problem op som mange mørke havde med den gule emoji. Jeg ville ikke umiddelbart selv mene at den gule emoji er diskirimerende fordi ingen mennesker er neongule i deres hudtone, men i og med at mange tolker på ALT så har jeg valgt at man selv kan tolke det man synes ud fra den gule/brune emoji. 

Jeg har forsøgt at lave en kode der tager fat i nogle af de ting vi har lært i ugens løb, men må indrømme jeg synes det en anelse svært med variable og if-statements. Men tænker at jo mere man arbejder med det, detsto mere giver det mening - håber jeg :) 


## Reference

https://p5js.org/reference/













