# ReadMe - MiniX1 


## Min kode
Min kode er simpel og består af en stribet baggrund med 5 smileys der bevæger sig op og ned ad **y-aksen.** 

Link til mit program: https://sofieamaliem.gitlab.io/aestetisk-programmering/MiniX1/index.html

Link til min kode: https://gitlab.com/sofieamaliem/aestetisk-programmering/-/blob/main/MiniX1/Modul1sketch.js 

![](/MiniX1/Skærmbillede_miniX1.png)


## Baggrund // Striber

Jeg har i min MiniX 1 lavet en kode, der består af en baggrund i lyserød og hvide striber, hvor striberne har en tynd sort kant, for at markere dem mere. Baggrunden har en grundfarve som jeg lavede jeg at bruge kodereferencen:  

`background(255, 153, 204);`

Her satte jeg mine valgte farve-værdier ind. For at lave den tynde sorte kant der markerer de hvide striber, brugte jeg kodereferencen: 

`strokeWeight ();` og satte den til **0.5**. 

Jeg lavede dernæst de hvide striber ved at bruge kodereferencen: 

`fill (255, 255, 255);`  

Jeg forsøgte mig dermed at lave en variabel hvor **x= 0** for at undgå at gentage min kode mange gange. Min **y-værdi** ændrede sig, med en stigning på **50** fra **0** til **900** for at skabe et ensartede mønster. Jeg satte derfor min **w-værdi** og **h-værdi** til det samme for hver stribede jeg lavede. 

## Ikoner // smiley
På striberne ses fem smileys, der graduerer i farven lyseblå til mørkeblå. Hver smiley er lavet i en **vidde (w)** på **175** og en **højde (h)** på **175** og er placeret forskelligt på canvas så de står lige hen ad **x-aksen.** Ved at bruge koden: 

`ellipse (x, y, w, [h]); `

Denne skaber grundlaget for udseendet på smileyen. I ellipse-reference står **x-koordinaten** for hvor henne cirklen er placeret på canvas i forholdt til **x-aksen**, altså hvor det første punkt er som også er center for cirklen. **Y-koordinaten** står ligeledes for hvor henne cirklen er placeret på canvas i forholdt til **y-aksen**, som er den vandrette placeret. **Vidden** eller **(w)** er altså vidden af cirklen som spiller sammen med **højden (h)** som kan være valgfrit. Jeg brugte højden til mine ellipser, for at forstå hvordan man skulle aflæse og placere geometriske former på canvas ved hjælp af kodningen. 

Som eksempel, hvis man ser på mine valgte koder til **// BLÅ SMILEY 1**, begyndte jeg med at udfylde min ellipse ved hjælp af farvekoder ved at bruge koden: 

`fill (0, 204, 255);` der står for henholdsvis (Rød, Grøn, Blå). 

Dernæst indtastede jeg de værdier jeg synes min ellipse skulle have for at skabe det ønskede design. Som man kan se i min kode, har jeg byttet min **y-værdi** ud med **y-variabel** i stedet. Dette vil jeg vende tilbage til. For dernæst at designe øjne til min smiley, har jeg igen brugt kodereferencen: 

`fill (255, 255, 255);` 

Som basisviden har farven hvid altid en værdi på **255** og sort har en værdi på **0**. For at få den samme form på mine øjne, har jeg brugt denne kodereference: 

`rect (x, y, w, [h], [tl]);` 

Hvor mine x og **y-værdier** er forskellige men min højde, vidde og radius af øverste venstre hjørne [tl] er ens. For at designe smileyens mund, har jeg brugt kodereferencen: 

`arc (x, y, h, start, stop, PI); `

Som ligesom de andre geometriske figurer, har **x, y, h-værdier**. Denne figur har også en **start-vinkel** for at starte buen, der er angivet i radianer, og en **stop-vinkel** for at stoppe buen, der ligeledes er specificeret i radianer. Nu er basisdelen af smileyen lavet, selve formen, øjnene og munden. For at lave nogle detaljer, tilføjede jeg sorte cirkler i øjnene. Jeg vidste hvor mine rektangler var placeret og kunne ud fra disse værdier, regne mig frem til hvor øjnene skulle placeres henne. Jeg brugte derfor kodereferencen 

`circle (x, y, d); `

hvor **d** står for cirklens diameter. Jeg valgte bevidst at placere det sorte cirkler i ydersiden af rektangel så de kiggede ind på smileyen i midten. Værdierne på disse er derfor en anelse skæve. Begge øjne i smileyen har en diameter på **12**. Jeg gentog dermed koden på alle mine smileyer og ændrede værdierne alt efter hvor på canvas de er placeret. 

## Bevægelse i min kode // y-aksen

Efter alle smileyerne er lavet, valgte jeg at der skulle tilføjes bevægelse. For at få bevægelse i ens design, skal der bruges en variabel. Jeg lavede dermed mine **y-værdier** til en variabel og satte dem til forskellige værdier afhængig af hvilken geometrisk figur det omhandler. Dette lavede jeg oven over function setup, hvor jeg delte mine **y-værdier** op til hver smiley. Når man laver en variabel, skal man nede hvor koden skriver til den bestemte figur, skrive eks. 

`y++; `

for at afslutte den variable kode. Hver enkel figur har fået en variabel, da jeg ikke har lavet smileyen som en gruppe. Hvis dette var tilfældet, kunne man på en nemmere og hurtigere få bevægelse. 

## Min oplevelse 

Den første oplevelse med kodning har været interessant. Det er sjovt at man går fra ikke at forstå hvordan man sætter en cirkel ind, til at kunne få dem til at bevæge sig og forstå placeringerne og dermed lave koden meget hurtigere. Jeg synes det er meget sjovere at man selv sidder og finder ud af hvordan de forskellige værdier vises inde på canvas og dermed prøver sig frem, i stedet for at kopierer en hel kode og ikke forstå processen bag ved. Samtidig med at det har været en sjov oplevelse har det også været frustrerende når tingene ikke har virket. Dette skyldes højst sandsynligvis at jeg endnu ikke forstår diverse fejlkoder og helt forstår hvordan en kode kan sammensættes. 

Der er også langt fra tanke til at kunne udføre det. Hvis man har en ide, skal der en del til for at kunne finde en måde hvorpå den ide kan visualiseres i kodningen. Dette kommer forhåbentligt med tiden, men for mig er det at skabe et design ved hjælp af kodning en proces man kan arbejde på over en længere periode. Der er stor forskel på når man sidder og læser en tekst og skriver en tekst til at du sidder og arbejder med koder. Når man sidder og læser og skriver, ligger det nært i ens måde at gøre ting på, og det kommer derfor naturligt til en. Kodning er ny læring og der skal derfor en hel del fejl til at man forstår hvordan man gør det rigtigt. En fejl kan gøre at hele koden ikke virker. En fejl i læsning eller skrivning har ikke konsekvenser. Kodning og programmering betyder at man kan forstå hvordan man skaber et visuelt design. Jeg får en helt anden forståelse for hvad design indebærer når man sidder og koder ens eget design fra bunden. For mig, betyder det, at når jeg kan kode en hjemmeside eller et kompliceret visuelt design, kan man alt inden for design, eller i hvert fald nå rigtig langt. 

Hvis jeg skulle lave ændringer i min kode, ville jeg få mine ikoner til at bevæge sig mere frit og ikke så statisk i en retning. Ligeledes vil jeg gruppere mine koder som gør det nemmere at holde styr på og processen bliver simplificeret. 

## Reference

https://p5js.org/reference/






