# ReadMe MiniX4 
_// Af Sofie-Amalie Møller_

### Dear Data' - Mit projekt

![](/MiniX4/Skærmbillede_minix4.png)

**Link til source code** : https://sofieamaliem.gitlab.io/aestetisk-programmering/MiniX4/index.html

**Link til min kode:** https://gitlab.com/sofieamaliem/aestetisk-programmering/-/blob/main/MiniX4/MiniX4.js 

## Tanken bag  

Tanken bag mit program er at lave data om til kunst. Der findes et hav af forskellige måder at indsamling data fra brugerne og jeg synes derfor det kunne være interessant at gøre data til noget brugbart for dem som dataen tages fra. Ofte som forbruger får vi ikke mulighed for at se hvad vores data bliver brugt til, da vi ikke ved hvad vi siger ’accepter’ til når knappen trykkes ned. Jeg har derfor taget udgangspunkt i både flere kunstværker der er lavet på netop denne måde, men også ideen fra ’bullet journaling’ hvor man selv ud fra et kreativt perspektiv ’tracker’ sin egen data i løbet af en dag. Mit projekt er derfor en mere kritisk vinkel på ’data capture’ da meningen er at man ikke ved hvad mit program gør, eller hvad man skaber ved at interagere med det og give programmet data ud fra ens handlinger. 

Jeg har valgt at kalde mit værk for ’Dear Data’ da det skal være et projekt hvor det er op til en selv hvad man vil bruge programmet til og hvilke former for data man ønsker at give den. For at perspektivere til vores undervisning, fik jeg min inspiration da vi talte om 'Phantom Kitty af Neil MacAloney', som skabte et værk som skulle skjule brugerens egen data ved at skrive en masse random søgeord og taste forskelligt så den falske data overskyggede den oprindelige brugbare data. 

_'This device, the Phantom Kitty, is a new tool to rein in Big Bro. It is a device that injects
a vast amount of misinformation into any internet activity tracking system from govern - ment or corporate internet surveillance. It can potentially render many of these collection systems useless without the need for users to set up complex VPNs or Proxy servers.'_


I reference til dette, så har jeg forsøgt mig at skabe et program der både indsamler og skjuler data, om dette er lykkedes kan diskuteres, da jeg ønskede at der skete mere i mit program, men da jeg valgte at lave mine knapper i for-loop og arrays, så gjorde det min ide mere besværlig. 

I mit værk har jeg lavet det meget simpelt, så det er op til brugen selv at skabe et værk ud fra deres data. Meningen er at man skal kunne bruge programmet som sin egen hverdag data-insamler. 'Dear Data' fungere som en dagbog, hvor du indtaster din daglige data ved at skifte farve på 'knapperne' og bestemmer herefter selv hvad de forskellige knapper skal symbolisere. På samme tid, skaber du også et kunstværk ud fra den data du giver til programmet. 

Ydermere, så det med at en knap, kan afforde på mange forskellige måder gør det interessant udelukkende at arbejde med den fordi den kan mange forskellige ting afhæning af hvilken bruger den henvender sig til: 

_This chapter begins with a relatively simple action like switching a device on or off — light, a kitchen appliance, and so on. Moreover a button is “seductive,” 5 with its immediate feedback and instantaneous gratification. It compels you to press it. Similarly in software and online platforms like Facebook, a button calls for interaction, inviting the user to click, and interact with it in binary states: like or not-like, accept or cancel._


## Min kode

Mit program består af en masse knapper, der har hver sin funktion og har oprindeligt hver sin betydning, men kan tolkes frit. Min kode er forholdsvis simpel hvor jeg gør brug af lokale og globale variabler, for-loops, arrays og de data capture koder vi har lært i den forrige undervisning. 
Da min kode er megeg simpel og ensformig, har jeg tager udgangspunkt i enkelte udsnit og vælger at gå i dybden med den. 

Mit primære data input i denne MiniX4 er musen og tastatur. Jeg synes det kunne være interessant og se hvad man kunne skabe ud fra blot de to datainput. Dette gjorde så også at min udførelse af min opgave bliv snæver da jeg ikke havde så mange forskellige funktioner.

Jeg tager udgangspunkt i knap 1, da alle knapper næsten er identiske i deres kode, men forskellig i placeringen på canvas. Da min ide var at skabe mange knapper, ville jeg gerne undgå at sidde og hard-code over 100 knapper. Jeg gjorde derfor brug af først globale variabler som jeg bruger i mit `for-loop` og `arrays`. Dermed kunne jeg loope igennem knapperne i mit array og dermed sætte deres størrelse, position `(x,y)` og farve. Jeg lavede først en lokal variabel for hver knap samt satte `x = 0` og `y = 5` som er knappenernes placeringen på mit canvas. I mit `for-loo`p har jeg en `variabel [i]` der fungere som tæller og dernæst en betingelse som fortæller hvornår min tæller, altså `[i]`, skal stoppe som jeg har sat til at være 100 for at være sikker på den gik helt ud af mit canvas. Dernæst har jeg min Itereration som gør at mit loop sættes i gang som jeg har defineret ved `++.` Da jeg gør brug af indeks i som jo er en lokal variabel, har jeg placeret alle de funktioner der hører til den rigtige knap, under det samme for-loop. 

```
let knap1 = [];
let x=0;
let y=5;

for (let i=0; i < 100; i++){
     knap1[i] = createButton("");
     knap1[i].position(x,y);
     knap1[i].style("background-color","#FFA500");
     knap1 [i].size(10,100);
     x += 20; //afstanden fra midten af min knap til midten af den næste knap
     
```
I mit udsnit fra min kode har jeg ligeledes brugt de forskellige referencer til henholdsvis at style mine knapper og vælge størrelsen. Da jeg har sat min knap i et for-loop har jeg blot sat min  knap1[i].position(x,y); til at være x,y og defineret placeringen i min globale variabel. 

```
knap1[i].mouseOut(resetFarve1);
knap1[i].mousePressed(skifteFarve1);
```      
```
function resetFarve1()
knap1[i].style("background-color","#7EC0EE");
```
```
function skifteFarve1()
knap1[i].style("background-color","#7EC0EE");
```
For dernæst at få mine knapper til at skifte farve, placerede jeg min mouseOut og mousePressed inde i mit for-loop, da jeg bruger `indeks [i]` som er en lokal variabel. ligeledes satte jeg mine egne funktioner hhv. function skifteFarve1 og function resetFarve1 i mit for-loop, og definerede hvilken farve der skulle være i ved hver handling. 

```
knap = createButton("DEAR DATA");
knap.position(650,390); 
knap.size (200,100);
knap.style("background-color","#0000FF")
knap.style("color","#ffffff")
```
For at lave min knap i midten brugte jeg blot sammme fremgangsmåde, men da jeg kun skulle bruge en, lavede jeg den uden for mine for-loop og brugte heller ikke arrays. 

```
function keyPressed(){
if(keyCode === 32){
     knap.style("transform","rotate(180deg)"); 
     }
 }
```
For at gøre min kode mere interaktiv, tilføjede jeg funktionen `function keyPressed()` så når man trykker på mellemrumstasten vender knappen sig 180 grader, som alt er placeret i et if-statement. 


## Reference

https://www.theatlantic.com/entertainment/archive/2015/05/the-rise-of-the-data-artist/392399/ 

https://thecuriousprofessor.com/2019/12/14/visualizing-data-through-art-2/ 

https://p5js.org/reference/

Soon, Winnie & Cox, Geoff - Aesthetic programming, A handbook of Software Studies
